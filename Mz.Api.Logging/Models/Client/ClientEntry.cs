﻿using System;

namespace Mz.Api.Logging.Models.Client
{
    public class ClientEntry
    {
        public ClientEntry(string clientName, byte[] password)
        {
            Id = Guid.NewGuid();
            Password = password;
            ClientName = clientName;
        }

        public Guid Id { get; set; }
        public byte[] Password { get; set; }
        public string ClientName { get; set; }
    }
}
