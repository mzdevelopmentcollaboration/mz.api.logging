﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;

namespace Mz.Api.Logging.Models.Client
{
    public sealed class ClientContext : DbContext
    {
        public ClientContext(DbContextOptions<ClientContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientEntry>().HasKey(x => x.ClientName);
            modelBuilder.Entity<SessionEntry>().HasKey(x => x.ClientName);
        }

        public DbSet<ClientEntry> ClientEntries { get; set; }
        public DbSet<SessionEntry> SessionEntries { get; set; }
    }
}
