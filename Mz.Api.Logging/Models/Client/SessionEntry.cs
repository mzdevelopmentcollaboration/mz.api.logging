﻿using System;

namespace Mz.Api.Logging.Models.Client
{
    public class SessionEntry
    {
        public SessionEntry(string clientName, Guid sessionToken)
        {
            Id = Guid.NewGuid();
            ClientName = clientName;
            SessionToken = sessionToken;
            UpdateDateTime = DateTime.UtcNow;
        }

        public Guid Id { get; set; }
        public string ClientName { get; set; }
        public Guid SessionToken { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}
