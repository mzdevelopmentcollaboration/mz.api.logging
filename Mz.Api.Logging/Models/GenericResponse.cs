﻿using System.Net;

namespace Mz.Api.Logging.Models
{
    public class GenericResponse
    {
        public GenericResponse(HttpStatusCode httpStatusCode,
            string statusMessage)
        {
            HttpStatusCode = httpStatusCode;
            StatusMessage = statusMessage;
        }

        public HttpStatusCode HttpStatusCode { get; }
        public string StatusMessage { get; }
    }
}
