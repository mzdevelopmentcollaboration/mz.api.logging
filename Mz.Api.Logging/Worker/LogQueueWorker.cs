﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using Mz.Api.Logging.Models.Log;

namespace Mz.Api.Logging.Worker
{
    public class LogQueueWorker : BackgroundService
    {
        private readonly LogContext _database;
        private readonly ILogger<LogQueueWorker> _logger;

        public LogQueueWorker(ILogger<LogQueueWorker> logger,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _database = serviceProvider.CreateScope().ServiceProvider
                .GetRequiredService<LogContext>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var logEntryItemCount = 0;
                while (Program.LogQueue.TryDequeue(out var logEntry))
                {
                    await _database.AddAsync(logEntry, stoppingToken);
                    logEntryItemCount++;
                }
                await _database.SaveChangesAsync(stoppingToken);
                if (logEntryItemCount != 0)
                {
                    _logger.LogInformation(logEntryItemCount + 
                        " Item/s has/have been written to the Database!");
                }
                await Task.Delay(2500, stoppingToken);
            }
        }
    }
}
