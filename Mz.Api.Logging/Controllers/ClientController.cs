﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;

using Mz.Api.Logging.Models;
using Mz.Api.Logging.Models.Client;

namespace Mz.Api.Logging.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class ClientController : Controller
    {
        private readonly ILogger<ClientController> _logger;
        private readonly ClientContext _databaseClientContext;

        public ClientController(ILogger<ClientController> logger,
            ClientContext databaseClientContext)
        {
            _logger = logger;
            _databaseClientContext = databaseClientContext;
        }

        [HttpPost]
        [Route("/api/[controller]/login")]
        public async Task<IActionResult> LoginClient()
        {
            var clientName = await AuthorizeClientAsync(Request.Headers);
            if (clientName == string.Empty)
            {
                return StatusCode(403, new GenericResponse(HttpStatusCode.Forbidden,
                    "The supplied Login-Data was invalid!"));
            }
            if (await _databaseClientContext.SessionEntries.AnyAsync(
                x => x.ClientName == clientName))
            {
                _databaseClientContext.SessionEntries.Remove(
                    await _databaseClientContext.SessionEntries.FindAsync(clientName));
                await _databaseClientContext.SaveChangesAsync();
            }
            var sessionToken = Guid.NewGuid();
            await _databaseClientContext.SessionEntries.AddAsync(new SessionEntry(
                clientName, sessionToken));
            await _databaseClientContext.SaveChangesAsync();
            //TODO: Create unique Response-Type
            return StatusCode(200, new GenericResponse(HttpStatusCode.OK,
                sessionToken.ToString()));

        }

        [HttpPost]
        [Route("/api/[controller]/register")]
        [Consumes("application/x-www-form-urlencoded")]
        public async Task<IActionResult> RegisterClient()
        {
            if (!Request.Form.ContainsKey("ClientName") ||
                !Request.Form.ContainsKey("Password")) return StatusCode(400,
                new GenericResponse(HttpStatusCode.BadRequest,
                "The supplied Data was invalid!"));
            if (await _databaseClientContext.ClientEntries.AnyAsync(
                x => x.ClientName == Request.Form["ClientName"][0]))
            {
                return StatusCode(409, new GenericResponse(HttpStatusCode.Conflict,
                    "A Client with this Name already exists!"));
            }
            using var hashBuilder = SHA256.Create();
            var passwordHash = hashBuilder.ComputeHash(
                Encoding.UTF8.GetBytes(Request.Form["Password"]));
            await _databaseClientContext.ClientEntries.AddAsync(
                new ClientEntry(Request.Form["ClientName"], passwordHash));
            await _databaseClientContext.SaveChangesAsync();
            return StatusCode(200, new GenericResponse(HttpStatusCode.OK,
                "Client successfully registered!"));
        }

        private async Task<string> AuthorizeClientAsync(IHeaderDictionary requestHeader)
        {
            if (!requestHeader.ContainsKey("Authorization") ||
                !requestHeader["Authorization"][0].Contains("Basic")) return string.Empty;
            var baseClientCredentials = Encoding.UTF8.GetString(
                Convert.FromBase64String(requestHeader["Authorization"][0]
                    .Substring("Basic ".Length).Trim()));
            var clientUsername = baseClientCredentials.Substring(0,
                baseClientCredentials.IndexOf(':'));
            using var hashBuilder = SHA256.Create();
            var passwordBytes = hashBuilder.ComputeHash(
                Encoding.UTF8.GetBytes(baseClientCredentials
                    .Substring(baseClientCredentials.IndexOf(':') + 1)));
            return await _databaseClientContext.ClientEntries.AnyAsync(
                x => x.ClientName == clientUsername &&
                     x.Password == passwordBytes) ? clientUsername : string.Empty;
        }
    }
}
