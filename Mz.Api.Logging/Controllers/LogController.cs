﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Mz.Api.Logging.Models;
using Mz.Api.Logging.Models.Log;
using static Mz.Api.Logging.Program;

namespace Mz.Api.Logging.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class LogController : ControllerBase
    {
        private readonly ILogger<LogController> _logger;

        public LogController(ILogger<LogController> logger)
        {
            _logger = logger;
        }

        //TODO: Change to Authorization-Value
        [HttpPost]
        public IActionResult PostLogEntry([FromBody] object information)
        {
            if (!Request.Headers.ContainsKey("ClientName"))
            {
                _logger.LogWarning("No Client-Header was provided by " +
                    $"{Request.HttpContext.Connection.RemoteIpAddress}!");
                return StatusCode(400, new GenericResponse(
                    HttpStatusCode.BadRequest, 
                    "The Client-Header was not supplied!"));
            }
            var clientName = Request.Headers["ClientName"];
            LogQueue.Enqueue(new LogEntry(clientName,
                information.ToString()));
            _logger.LogInformation($"A Log-Entry was provided by {clientName}!");
            return StatusCode(200, new GenericResponse(HttpStatusCode.OK,
                "Your Log-Entry was saved!"));
        }
    }
}
