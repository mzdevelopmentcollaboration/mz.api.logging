using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Mz.Api.Logging.Worker;
using Mz.Api.Logging.Models.Log;
using Mz.Api.Logging.Models.Client;

namespace Mz.Api.Logging
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<LogContext>(options =>
            {
                var dbConnectionString =
                    Environment.GetEnvironmentVariable("logConnectionString");
                options.UseMySql(dbConnectionString ?? throw new InvalidOperationException(
                        "No Log-Connection-String was found!"),
                    ServerVersion.AutoDetect(dbConnectionString));
            });
            services.AddDbContext<ClientContext>(options =>
            {
                var dbConnectionString =
                    Environment.GetEnvironmentVariable("clientConnectionString");
                options.UseMySql(dbConnectionString ?? throw new InvalidOperationException(
                        "No Client-Connection-String was found!"),
                    ServerVersion.AutoDetect(dbConnectionString));
            });
            services.AddHostedService<LogQueueWorker>();
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
